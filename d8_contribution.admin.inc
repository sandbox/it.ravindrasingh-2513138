<?php

/**
 * @file
 * D8 Configuration form for setting your d.o market place URL.
 */

/**
 * Settings page.
 */
function d8_contribution_admin($form, &$form_state) {
  
  $form = array();
  $form['d8_contribution_matketplace_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Marketplace URL from drupal.org'),
    '#description' => t('Name of your organization'),
    '#default_value' => variable_get('d8_contribution_matketplace_name', NULL),
  );
  $form['d8_contribution_matketplace_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Marketplace URL from drupal.org'),
    '#description' => t('Copy your company URL from drupal.org. 
     e.g https://www.drupal.org/marketplace/srijan-technologies'),
    '#default_value' => variable_get('d8_contribution_matketplace_url', NULL),
  );
  
  return system_settings_form($form);
}
